using UnityEngine;using UnityEngine.UI;

public class MultiplicationTableGenerator : MonoBehaviour {

    [SerializeField] private Text multiplicationTableResultText;
    
    public void GenerateMultiplicationTable()
    {
        var result = "";

        for (var i = 1; i <= 10; i++)
        {
            for (var j = 1; j <= 10; j++)
            {
                result += $"{j}*{i}={j * i}\t";
            }

            if (i == 10)
            {
                result += "\n"; 
            }
        }

        multiplicationTableResultText.text = result;

        Debug.Log($"Таблица умножения: \n{result}");
    }
}
