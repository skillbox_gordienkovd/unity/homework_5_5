using UnityEngine;
using UnityEngine.UI;

public class FactorialOfNumberCalculator : MonoBehaviour
{
    [SerializeField] private InputField numberInput;
    [SerializeField] private Text factorialOfNumberResultText;

    
    public void CalculateFactorialOfNumber()
    {
        var isSuccessParsedInputNumberText = int.TryParse(numberInput.text, out var inputNumber);

        if (!isSuccessParsedInputNumberText || inputNumber < 1)
        {
            factorialOfNumberResultText.text = "Число введено ошибочно";
            
            return;
        }

        var result = 1;

        for (var i = 1; i <= inputNumber; i++)
        {
            result *= i;
        }

        factorialOfNumberResultText.text = $"{result}";

        Debug.Log($"Факториал числа {inputNumber} = {result}");
    }
    
    /**
     * Можно вызвать кликом на кнопку, но значение параметра
     * будет задано по умолчанию, передать введенное в input значение нельзя.
     * 
     * При этом вызывать метод с более чем одним параметром кликом по кнопке тоже нельзя.
     */
    public void CalculateFactorialOfNumber(int inputNumber)
    {
        
    }
}
