using UnityEngine;
using UnityEngine.UI;

public class EvenNumbersGenerator : MonoBehaviour
{
    [SerializeField] private InputField fromNumberInputField;
    [SerializeField] private InputField toNumberInputField;
    [SerializeField] private Text evenNumbersResultText;

    public void GenerateEvenNumbers()
    {
        GenerateEvenNumbers(fromNumberInputField.text, toNumberInputField.text);
    }
    private void GenerateEvenNumbers(
        string fromNumberText,
        string toNumberText
    )
    {
        var isSuccessParsedInputNumbers =
            TryParseIntInputNumbers(
                fromNumberText,
                toNumberText,
                out var fromNumber,
                out var toNumber
            );
        if (!isSuccessParsedInputNumbers)
        {
            evenNumbersResultText.text = "Числа введены ошибочно";
            
            return;
        }
        
        var result = "";

        for (var i = fromNumber; i < toNumber; i++)
        {
            if (i % 2 == 0)
            {
                result += $"{i} ";
            }
        }

        evenNumbersResultText.text = result;

        Debug.Log($"Четные числа от {fromNumber} до {toNumber}: {result}");
    }

    private static bool TryParseIntInputNumbers(
        string fromNumberText,
        string toNumberText,
        out int fromNumber,
        out int toNumber
    )
    {
        var isSuccessParsedFromNumberText = int.TryParse(fromNumberText, out fromNumber);
        var isSuccessParsedToNumberText = int.TryParse(toNumberText, out toNumber);

        return isSuccessParsedFromNumberText && isSuccessParsedToNumberText;
    }
}